<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    /**
     * Show All Users
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('users.index', ['users' => User::with('wallet')->get()]);
    }

    /**
     * Show User's profile.
     * @param User $user
     * @return \Illuminate\View\View
     */
    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }

    /**
     * Check if the Email exists in our database of not
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function checkEmail(Request $request)
    {
        if ((User::where('email', $request->email)->exists()) && ($request->email != auth()->user()->email)) {
            return response('', 201);
        }
        return response('', 422);
    }
}
