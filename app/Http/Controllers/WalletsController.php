<?php

namespace App\Http\Controllers;

use App\Wallet;
use Illuminate\Http\Request;

class WalletsController extends Controller
{

    /**
     * Get all withdrawals.
     * @param Wallet $wallet
     * @return \Illuminate\View\View
     */
    public function withdrawals(Wallet $wallet)
    {
        $data = [
            'wallet'      => $wallet,
            'withdrawals' => $wallet->withdrawals
        ];
        return view('wallets.withdrawals', $data);
    }
    
    /**
     * Get all deposits.
     * @param Wallet $wallet
     * @return \Illuminate\View\View
     */
    public function deposits(Wallet $wallet)
    {
        $data = [
            'wallet'      => $wallet,
            'deposits' => $wallet->deposits
        ];
        return view('wallets.deposits', $data);
    }

    /**
     * Get all sent and received transactions for specific wallet
     * @param Wallet $wallet
     * @return \Illuminate\View\View
     */
    public function walletTransactions(Wallet $wallet)
    {
        $data = [
            'wallet'                => $wallet,
            'sent_transactions'     => $wallet->sentTransactions,
            'recieved_transactions' => $wallet->receivedTransactions,
        ];
        return view('wallets.transactions', $data);
    }

    /**
     * Transfer from Wallet to another by the E-mail.
     * @param intger $sender_wallet_id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function walletTransfer($sender_wallet_id, Request $request)
    {
        $sender_wallet = Wallet::findOrFail($sender_wallet_id);
        $rules = [
            'email'  => [
                'required',
                'exists:users.email'
            ],
            'amount' => ['required', 'numeric', 'max:' . $sender_wallet->ballance]
        ];
        validator($request->all(), $rules);

        $data = $request->only(array_keys($rules));

        $auth_user = auth()->user();

        if ($request->email === $auth_user->email) {
            return response('email error', 422);
        }

        $received_wallet = \App\User::where('email', $data['email'])->firstOrFail()->wallet;

        $new_sender_balance = $sender_wallet->balance - (int) $data['amount'];
        $received_wallet->balance = $received_wallet->balance + (int) $data['amount'];

        $sender_wallet->update(['balance' => $new_sender_balance]);
        $received_wallet->saveOrFail();

        \App\Transaction::create([
            'wallet_send_id'    => $sender_wallet->id,
            'wallet_receive_id' => $received_wallet->id,
            'amount'            => $data['amount']
        ]);
        return response('', 202);
    }

    /**
     * Withdraw from Wallet
     * @param Wallet $wallet
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function withdraw(Wallet $wallet, Request $request)
    {
        $rules = [
            'amount' => ['required', 'numeric', 'min:1', 'max:' . $wallet->ballance]
        ];
        validator($request->all(), $rules);
        
        $new_balance = $wallet->balance - $request->amount;

        if ($wallet->update(['balance'=>$new_balance])) {
            $wallet->withdrawals()->create(['amount'=>$request->amount]);
            return response('', 202);
        }
        return response('internal error', 500);
    }

    /**
     * Deposit to Wallet
     * @param Wallet $wallet
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function deposit(Wallet $wallet, Request $request)
    {
        $rules = [
            'amount' => ['required', 'numeric', 'min:1']
        ];
        validator($request->all(), $rules);
        
        $new_balance = $wallet->balance + $request->amount;

        if ($wallet->update(['balance'=>$new_balance])) {
            $wallet->deposits()->create(['amount'=>$request->amount]);
            return response('', 202);
        }
        return response('internal error', 500);
    }
}
