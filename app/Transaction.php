<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'transactions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'wallet_send_id',
        'wallet_receive_id',
        'amount',
    ];

    /**
     * Relationship between Transaction and Wallet Models (many-to-one).
     * Received Wallet.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function receivedWallet()
    {
        return $this->belongsTo(Wallet::class, 'wallet_receive_id', 'id');
    }

    /**
     * Relationship between Transaction and Wallet Models (many-to-one).
     * Sent Wallet.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sentWallet()
    {
        return $this->belongsTo(Wallet::class, 'wallet_send_id', 'id');
    }
}
