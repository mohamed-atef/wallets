<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wallets';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'balance',
    ];

    /**
     * Relationship method between Wallet and User Models (one-to-one)
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Relationship method between Wallet and Transaction Models (one-to-many)
     * Getting the Transactions that have been sent by Wallet.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sentTransactions()
    {
        return $this->hasMany(Transaction::class, 'wallet_send_id', 'id');
    }

    /**
     * Relationship method between Wallet and Transaction Models (one-to-many)
     * Getting the Transactions that have been sent by Wallet.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function receivedTransactions()
    {
        return $this->hasMany(Transaction::class, 'wallet_receive_id', 'id');
    }

    /**
     * Relationship method between Wallet and Withdrawal Models (one-to-many)
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function withdrawals()
    {
        return $this->hasMany(Withdrawal::class, 'wallet_id', 'id');
    }

    /**
     * Relationship method between Wallet and Deposit Models (one-to-many)
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deposits()
    {
        return $this->hasMany(Deposit::class, 'wallet_id', 'id');
    }
}
