<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

     /**
     * Determine whether the user can view the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $profile
     * @return mixed
     */
    public function view(User $user, User $profile)
    {
        return $this->profileOwner($user, $profile);
    }

    /**
     * Check the profile owner.
     * @param User $user
     * @param User $profile
     * @return boolean
     */
    private function profileOwner(User $user, User $profile)
    {
        if (($user->id === $profile->id)) {
            return true;
        }
        return false;
    }
}
