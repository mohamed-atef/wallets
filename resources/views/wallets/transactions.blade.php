@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                Make New Transaction
            </button>
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">New Transaction</h4>
                        </div>
                        <div class="modal-body">
                            <new-transaction 
                                wallet="{{$wallet}}"
                                current-user-email="{{auth()->user()->email}}"
                                >
                            </new-transaction>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>Sent Transactions</h1>
            <table class="table table-bordered table-hover">
                <thead class="thead-default">
                    <tr>
                        <th>To</th>
                        <th>Amount</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sent_transactions as $sent_transaction)
                    <tr>
                        <th scope="row">{{$sent_transaction->receivedWallet->user->email}}</th>
                        <td>{{$sent_transaction->amount}}</td>
                        <td>{{$sent_transaction->created_at}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>Recieved Transactions</h1>
            <table class="table table-bordered table-hover">
                <thead class="thead-default">
                    <tr>
                        <th>From</th>
                        <th>Amount</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($recieved_transactions as $recieved_transaction)
                    <tr>
                        <th scope="row">{{$recieved_transaction->sentWallet->user->email}}</th>
                        <td>{{$recieved_transaction->amount}}</td>
                        <td>{{$recieved_transaction->created_at}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection


