@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <table class="table table-bordered table-hover">
                <thead class="thead-default">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>E-mail</th>
                        <th>Balance</th>
                        <th>Transactions</th>
                        <th>Withdrawals</th>
                        <th>Deposits</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">{{$user->id}}</th>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->wallet->balance}}</td>
                        <td>
                            <a 
                                class="btn btn-default" 
                                href="/wallets/transactions/{{$user->wallet->id}}" 
                                role="button"
                                >
                                Transactions
                            </a>
                        </td>
                        <td>
                            <a 
                                class="btn btn-primary" 
                                href="{{secure_url('wallets/withdrawals', $user->wallet->id)}}" 
                                role="button"
                                >
                                Withdrawals
                            </a>
                        </td>
                        <td>
                            <a 
                                class="btn btn-info" 
                                href="{{secure_url('wallets/deposits', $user->wallet->id)}}" 
                                role="button"
                                >
                                Deposits
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

