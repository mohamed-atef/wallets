<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('/users')->group(function() {
    Illuminate\Routing\Router::get('/', 'UsersController@index');
    Illuminate\Routing\Router::get('/{user}',
        [
        'middleware' => ['can:view,user'],
        'uses'       => 'UsersController@show',
        'as'         => 'user.profile'
        ]
    );
    Route::post('/check-email', 'UsersController@checkEmail');
});

Route::prefix('/wallets')->group(function() {
    Route::get('/transactions/{wallet}', 'WalletsController@walletTransactions');
    Route::get('/withdrawals/{wallet}', 'WalletsController@withdrawals');
    Route::get('/deposits/{wallet}', 'WalletsController@deposits');
    Route::put('/transfer/{wallet}', 'WalletsController@walletTransfer');
    Route::put('/withdraw/{wallet}', 'WalletsController@withdraw');
    Route::put('/deposit/{wallet}', 'WalletsController@deposit');
});
